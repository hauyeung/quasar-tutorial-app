import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './quasar'
import { formatDate } from './filters/formatDate';
import { capitalize } from './filters/capitalize';

Vue.config.productionTip = false

Vue.filter('formatDate', formatDate);
Vue.filter('capitalize', capitalize);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
